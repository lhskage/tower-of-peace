package main.java.spells.abstractions;

public enum SpellType {
    ArcaneMissile,
    Barrier,
    ChaosBolt,
    FlashHeal,
    IronBark,
    Regrowth
}
