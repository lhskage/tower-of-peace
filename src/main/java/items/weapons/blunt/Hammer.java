package main.java.items.weapons.blunt;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Hammer extends Weapon {
    public Hammer(double rarity) {
        super(rarity,
                WeaponStatsModifiers.HAMMER_ATTACK_MOD);
    }
}
