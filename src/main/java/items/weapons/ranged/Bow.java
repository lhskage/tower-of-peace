package main.java.items.weapons.ranged;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Bow extends Weapon {
    // Constructors
    public Bow(double rarity) {
        super(rarity, WeaponStatsModifiers.BOW_ATTACK_MOD);
    }
}
