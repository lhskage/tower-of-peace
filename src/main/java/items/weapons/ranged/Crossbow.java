package main.java.items.weapons.ranged;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Crossbow extends Weapon {
    public Crossbow(double rarity) {
        super(rarity,
                WeaponStatsModifiers.CROSSBOW_ATTACK_MOD);
    }
}
