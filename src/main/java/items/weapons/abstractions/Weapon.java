package main.java.items.weapons.abstractions;

public abstract class Weapon {

    // Variables
    protected double rarity;
    protected double attackPowerModifier;

    // Constructor
    public Weapon(double rarity,
                  double attackPowerModifier) {
        this.rarity = rarity;
        this.attackPowerModifier = attackPowerModifier;
    }

    // Public properties
    public double getRarity() {
        return rarity;
    }
    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

}
