package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Wand extends Weapon {
    public Wand(double rarity) {
        super(rarity,
                WeaponStatsModifiers.WAND_MAGIC_MOD);
    }
}
