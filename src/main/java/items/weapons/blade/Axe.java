package main.java.items.weapons.blade;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Axe extends Weapon {
    public Axe(double rarity) {
        super(rarity, WeaponStatsModifiers.AXE_ATTACK_MOD);
    }
}