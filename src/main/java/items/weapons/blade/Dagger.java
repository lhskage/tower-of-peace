package main.java.items.weapons.blade;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Dagger extends Weapon {
    public Dagger(double rarity) {
        super(rarity,
                WeaponStatsModifiers.DAGGER_ATTACK_MOD);
    }
}
