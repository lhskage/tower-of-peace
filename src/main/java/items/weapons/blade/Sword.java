package main.java.items.weapons.blade;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Sword extends Weapon {
    public Sword(double rarity) {
        super(rarity,
                WeaponStatsModifiers.SWORD_ATTACK_MOD);
    }
}
