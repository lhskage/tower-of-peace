package main.java.items.armor.abstractions;

public enum ArmorType {
    Cloth,
    Leather,
    Mail,
    Plate
}
