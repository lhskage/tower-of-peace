package main.java.items.armor.abstractions;

public class Armor {
    // Modifiers
    protected double rarityModifier;
    protected double healthModifier;
    protected double physRedModifier;
    protected double magicRedModifier;

    // Constructors
    public Armor(double itemRarity, double healthModifier,
                 double physRedModifier, double magicRedModifier) {
        this.rarityModifier = itemRarity;
        this.healthModifier = healthModifier;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
    }

    // Public properties
    public double getRarityModifier() {
        return rarityModifier;
    }

    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

}
