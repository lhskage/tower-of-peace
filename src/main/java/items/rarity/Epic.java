package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Epic {
    // Stat modifier
    private double powerModifier = 1.6;
    // Color for display purposes
    private String itemRarityColor = Color.MAGENTA;
}
