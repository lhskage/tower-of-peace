package main.java.characters.abstractions;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

public abstract class Caster extends Character {
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Caster;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;

    protected DamagingSpell damagingSpell;

    protected double baseMagicPower;

    // Constructor
    public Caster(double baseHealth,
                  double basePhysicalReductionPercent,
                  double baseMagicReductionPercent,
                  double baseMagicPower) {
        super(baseHealth,
                basePhysicalReductionPercent,
                baseMagicReductionPercent);
        this.baseMagicPower = baseMagicPower;
    }

    // Character behaviours
    public double castDamagingSpell() {
        return baseMagicPower * damagingSpell.getSpellDamageModifier() * damagingSpell.getSpellDamageModifier();
    }
}
