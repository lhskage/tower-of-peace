package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Support extends Character{
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Support;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;

    public Support(double baseHealth,
                   double basePhysicalReductionPercent,
                   double baseMagicReductionPercent) {
        super(baseHealth,
                basePhysicalReductionPercent,
                baseMagicReductionPercent);
    }
}