package main.java.characters.abstractions;

import main.java.basestats.ItemRarityModifiers;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public abstract class Character {
    // Active trackers
    protected double currentHealth;
    protected Boolean isDead = false;
    protected Weapon weapon;
    protected Armor armor;
    protected double baseHealth, basePhysicalReductionPercent,
            baseMagicReductionPercent;

    // For creating Caster and Ranged characters
    public Character(double baseHealth,
                     double basePhysicalReductionPercent,
                     double baseMagicReductionPercent) {
        this.baseHealth = baseHealth;
        this.basePhysicalReductionPercent = basePhysicalReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
        this.currentHealth = baseHealth;
    }

    // Public Getters
    public double getCurrentMaxHealth() {
        if(this.armor != null) {
            this.baseHealth *= this.armor.getHealthModifier();
        }
        return baseHealth;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        if(currentHealth <= 0) {
            isDead = true;
            return true;
        }
        return false;
    }

    public double takeDamage(double incomingDamage, String damageType) {
        double dmgTaken = 0;
        if(damageType == "Magic") {
            dmgTaken = incomingDamage * (1-(baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if(damageType == "Physical") {
            dmgTaken = incomingDamage * (1-(basePhysicalReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        }
        return dmgTaken;
    }

    // Equips a weapon to the character, modifying stats
    public void equipWeapon(WeaponType weaponType, double itemRarity) {
        WeaponFactory weaponFactory = new WeaponFactory();
        this.weapon = weaponFactory.getItem(weaponType, itemRarity);
    }

    public void equipArmor(ArmorType armorType, double itemRarity) {
        ArmorFactory armorFactory = new ArmorFactory();
        this.armor = armorFactory.getArmor(armorType, itemRarity);
    }
}