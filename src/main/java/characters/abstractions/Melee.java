package main.java.characters.abstractions;

public abstract class Melee extends Character{
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Melee;

    protected double baseAttackPower;

    public Melee(double baseHealth,
                 double basePhysicalReductionPercent,
                 double baseMagicReductionPercent,
                 double baseAttackPower) {
        super(baseHealth,
                basePhysicalReductionPercent,
                baseMagicReductionPercent);
        this.baseAttackPower = baseAttackPower;
    }
}
