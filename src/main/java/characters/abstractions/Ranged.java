package main.java.characters.abstractions;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Ranged extends Character {
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Ranged;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Ranged;
    public final ArmorType ARMOR_TYPE = ArmorType.Mail;

    protected double baseAttackPower;

    public Ranged(double baseHealth,
                  double basePhysicalReductionPercent,
                  double baseMagicReductionPercent,
                  double baseAttackPower) {
        super(baseHealth,
                basePhysicalReductionPercent,
                baseMagicReductionPercent);
        this.baseAttackPower = baseAttackPower;
    }

    public double attackWithRangedWeapon() {
        return baseAttackPower * weapon.getAttackPowerModifier() * weapon.getRarity(); // Replaced with actual damage amount based on calculations
    }
}