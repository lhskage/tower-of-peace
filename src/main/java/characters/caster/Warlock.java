package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.spells.abstractions.DamagingSpell;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Caster {
    public Warlock(DamagingSpell damagingSpell) {
        super(CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER);
        this.damagingSpell = damagingSpell;
    }
}
