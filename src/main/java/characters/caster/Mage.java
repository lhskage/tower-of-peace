package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.spells.abstractions.DamagingSpell;
/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Caster {
    public Mage(DamagingSpell damagingSpell) {
        super(CharacterBaseStatsDefensive.MAGE_BASE_HEALTH,
                CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.MAGE_MAGIC_POWER);
        this.damagingSpell = damagingSpell;
    }
}
