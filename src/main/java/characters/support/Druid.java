package main.java.characters.support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.Support;
import main.java.items.armor.abstractions.ArmorType;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends Support {

    protected final ArmorType ARMOR_TYPE = ArmorType.Leather;

    protected HealingSpell healingSpell;

    public Druid(HealingSpell healingSpell) {
        super(CharacterBaseStatsDefensive.DRUID_BASE_HEALTH,
                CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES);
        this.healingSpell = healingSpell;
    }

    // Character behaviours
    // Heals the party member, increasing their current health.
    public void healPartyMember(Character partyMember) {
        
    }
}
