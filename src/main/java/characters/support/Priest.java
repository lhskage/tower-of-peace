package main.java.characters.support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Support;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends Support {

    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;

    private ShieldingSpell shieldingSpell;

    // Constructor
    public Priest(ShieldingSpell shieldingSpell) {
        super(CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH,
                CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES);
        this.shieldingSpell = shieldingSpell;
    }

    // Shields a party member for a percentage of their maximum health.
    public double shieldPartyMember(double partyMemberMaxHealth) {
        double output = (partyMemberMaxHealth * shieldingSpell.getAbsorbShieldPercentage());
        return 0;
    }
}
