package main.java.characters.ranged;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Ranged;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Ranged {
    public Ranger() {
        super(CharacterBaseStatsDefensive.RANGER_BASE_HEALTH,
                CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER);
    }
}
