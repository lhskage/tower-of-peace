package main.java;


import main.java.basestats.ItemRarityModifiers;
import main.java.characters.ranged.Ranger;
import main.java.characters.support.Druid;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.healing.SwiftMend;

public class Main {
    public static void main(String[] args) {
        Ranger ranger = new Ranger();
        ranger.equipWeapon(WeaponType.Bow, ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        System.out.println("RANGER ATTACKED FOR " + ranger.attackWithRangedWeapon());

        Druid druid = new Druid(new SwiftMend());
        druid.equipArmor(ArmorType.Cloth, ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        druid.equipWeapon(WeaponType.Staff, ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        System.out.println("DRUID HEALTH: " + druid.getCurrentMaxHealth());
        System.out.println("");
        System.out.println("DMG TAKEN: " + druid.takeDamage(100, "Fire"));
        System.out.println("NEW HEALTH: " + druid.getCurrentHealth());
    }
}
